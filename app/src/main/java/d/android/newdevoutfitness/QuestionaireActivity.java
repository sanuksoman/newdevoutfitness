package d.android.newdevoutfitness;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class QuestionaireActivity extends AppCompatActivity
implements FirstQuestionFragment.OnFragmentInteractionListener,
SecondQuestionFragment.OnFragmentInteractionListener,
ThirdQuestionFragment.OnFragmentInteractionListener,
FourthQuestionFragment.OnFragmentInteractionListener,
FifthQuestionFragment.OnFragmentInteractionListener,
SixthQuestionFragment.OnFragmentInteractionListener{

    int i = 0;
    Button continue_btn, back_btn;
    TextView question_no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionaire);

        continue_btn = findViewById(R.id.continue_btn);
        back_btn = findViewById(R.id.back_btn);

        question_no = findViewById(R.id.question_no);
        question_no.setText("");

        FirstQuestionFragment firstQuestionFragment = new FirstQuestionFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_layout, firstQuestionFragment, firstQuestionFragment.getTag())
                .commit();

        continue_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(i<6){
                    i+=1;
                    if(i==6){i=5;}
                    check_fragment(i);
                }
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(i>0) {
                    i-=1;
                    check_fragment(i);
                }
            }
        });
    }

    private void check_fragment(int i) {
        switch (i){
            case 0:
                question_no.setText("");
                FirstQuestionFragment firstQuestionFragment = new FirstQuestionFragment();
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, firstQuestionFragment, firstQuestionFragment.getTag())
                        .commit();
                break;
            case 1:
                question_no.setText("1 of 5");
                SecondQuestionFragment secondQuestionFragment = new SecondQuestionFragment();
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, secondQuestionFragment, secondQuestionFragment.getTag())
                        .commit();
                break;
            case 2:
                question_no.setText("2 of 5");
                ThirdQuestionFragment thirdQuestionFragment = new ThirdQuestionFragment();
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, thirdQuestionFragment, thirdQuestionFragment.getTag())
                        .commit();
                break;
            case 3:
                question_no.setText("3 of 5");
                FourthQuestionFragment fourthQuestionFragment = new FourthQuestionFragment();
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fourthQuestionFragment, fourthQuestionFragment.getTag())
                        .commit();
                break;
            case 4:
                question_no.setText("4 of 5");
                FifthQuestionFragment fifthQuestionFragment = new FifthQuestionFragment();
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, fifthQuestionFragment, fifthQuestionFragment.getTag())
                        .commit();
                break;
            case 5:
                question_no.setText("5 of 5");
                SixthQuestionFragment sixthQuestionFragment = new SixthQuestionFragment();
                fragmentManager = getSupportFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.fragment_layout, sixthQuestionFragment, sixthQuestionFragment.getTag())
                        .commit();
                break;
            default:
                    Intent intent = new Intent(QuestionaireActivity.this, UserHomeActivity.class);
                    startActivity(intent);
        }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
