package d.android.newdevoutfitness;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public static final String PREF_USER_FIRST_TIME = "user_first_time";
    boolean isUserFirstTime;
    Button create_account_button, login_button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isUserFirstTime = Boolean.valueOf(Utils.readSharedSetting(MainActivity.this, PREF_USER_FIRST_TIME, "true"));

        Intent onboarding_intent = new Intent(MainActivity.this, OnBoardingActivity.class);
        onboarding_intent.putExtra(PREF_USER_FIRST_TIME, isUserFirstTime);

        if (isUserFirstTime)
            startActivity(onboarding_intent);

        setContentView(R.layout.activity_main);
        create_account_button = findViewById(R.id.create_account_button);
        login_button = findViewById(R.id.login_button);

        create_account_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginsignup = new Intent(MainActivity.this, LoginSignup.class);
                loginsignup.putExtra("action","signup");
                startActivity(loginsignup);
            }
        });

        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginsignup = new Intent(MainActivity.this, LoginSignup.class);
                loginsignup.putExtra("action","login");
                startActivity(loginsignup);
            }
        });
    }
}
