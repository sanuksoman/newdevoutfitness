package d.android.newdevoutfitness;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import org.w3c.dom.Text;

import java.util.Objects;

public class UserHomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_home);

        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        Intent intent = getIntent();
        final String user_id = intent.getStringExtra("uid");
        final String[] user_name = new String[1];
        final String[] user_email = new String[1];

        final TextView user_name_text = findViewById(R.id.user_name_text);
        final TextView user_email_id = findViewById(R.id.user_email_id);

        CollectionReference users_ref = db.collection("users");
        final Query get_user_details =  users_ref.whereEqualTo("uid",user_id);
        get_user_details.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if(task.isSuccessful()){
                    for(DocumentSnapshot documentSnapshot : Objects.requireNonNull(task.getResult())){
                        user_name[0] = documentSnapshot.getString("uname");
                        user_email[0] = documentSnapshot.getString("email");
                        user_name_text.setText(user_name[0]);
                        user_email_id.setText(user_email[0]);
                    }
                }
                else{
                    Toast.makeText(getApplicationContext(),"Error!",Toast.LENGTH_LONG).show();
                }
            }
        });


    }
}
