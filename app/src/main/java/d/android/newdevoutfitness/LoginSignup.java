package d.android.newdevoutfitness;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class LoginSignup extends AppCompatActivity {
    String action, email_login, password_login, username_signup, email_signup, password_signup;
    int count;
    Long first_time;
    private FirebaseAuth mAuth;
    private  FirebaseAuth.AuthStateListener mAuthListner;

    @Override
    protected void onStart() {
        super.onStart();
//        mAuth.addAuthStateListener(mAuthListner);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup);

        mAuth = FirebaseAuth.getInstance();
        final FirebaseFirestore db = FirebaseFirestore.getInstance();

        final ProgressBar progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.INVISIBLE);

        final RelativeLayout login = findViewById(R.id.login_fragment);
        final RelativeLayout signup = findViewById(R.id.signup_fragment);

        final EditText email_login_textbox = findViewById(R.id.email_login_textbox);
        final EditText password_login_textbox = findViewById(R.id.password_login_textbox);

        final EditText signup_username = findViewById(R.id.signup_username);
        final EditText signup_email = findViewById(R.id.signup_email);
        final EditText signup_password = findViewById(R.id.signup_password);

        Button logn_button_logintab = findViewById(R.id.login_button_logintab);
        Button signup_button_logintab = findViewById(R.id.signup_button_logintab);
        Button login_button_signuptab = findViewById(R.id.login_button_signuptab);
        Button signup_button_signuptab = findViewById(R.id.signup_button_signuptab);

        Button log_in_button = findViewById(R.id.log_in_button);
        Button continue_facebook_button = findViewById(R.id.continue_facebook_button);
        Button forgot_password_button = findViewById(R.id.forgot_password_button);
        Button signup_facebook = findViewById(R.id.signup_facebook);
        Button local_signup_button = findViewById(R.id.local_signup_button);

        log_in_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                email_login = email_login_textbox.getText().toString();
                password_login = password_login_textbox.getText().toString();

                if (TextUtils.isEmpty(email_login)) {
                    Toast.makeText(getApplicationContext(), "Please enter email id", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email_login).matches()){
                    Toast.makeText(getApplicationContext(), "Please enter valid email id", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                if (TextUtils.isEmpty(password_login)) {
                    Toast.makeText(getApplicationContext(), "Please enter password", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                mAuth.signInWithEmailAndPassword(email_login,password_login)
                        .addOnCompleteListener(LoginSignup.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progressBar.setVisibility(View.GONE);
                                if(task.isSuccessful()){
                                    CollectionReference users_ref = db.collection("users");
                                    final Query get_user_details =  users_ref.whereEqualTo("uid",mAuth.getUid());
                                    get_user_details.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if(task.isSuccessful()){
                                                for(DocumentSnapshot documentSnapshot : Objects.requireNonNull(task.getResult())){
                                                    first_time = documentSnapshot.getLong("first_time");
                                                }
                                                assert first_time != null;
                                                if(first_time == 1){
                                                    Intent user_home_intent = new Intent(LoginSignup.this, QuestionaireActivity.class);
                                                    user_home_intent.putExtra("uid",mAuth.getUid());
                                                    startActivity(user_home_intent);
                                                }
                                                else{
                                                    Intent user_home_intent = new Intent(LoginSignup.this, UserHomeActivity.class);
                                                    user_home_intent.putExtra("uid",mAuth.getUid());
                                                    startActivity(user_home_intent);
                                                }
                                            }
                                            else{
                                                Toast.makeText(getApplicationContext(),"Error!",Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                                }
                                else{
                                    Toast.makeText(getApplicationContext(), "Invalid credentials. Please try again",Toast.LENGTH_LONG).show();
                                }
                            }
                        });
//                mAuthListner = new FirebaseAuth.AuthStateListener() {
//                    @Override
//                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
//                        if (firebaseAuth.getCurrentUser() != null) {
//                            startActivity(new Intent(LoginSignup.this, UserHomeActivity.class));
//                        }
//                    }
//                };

            }
        });

        continue_facebook_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Continue with fb button", Toast.LENGTH_LONG).show();
            }
        });

        forgot_password_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Forgot Password Button", Toast.LENGTH_LONG).show();
            }
        });


        signup_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Signup with Facebook", Toast.LENGTH_LONG).show();
            }
        });
//                .get()
//                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
//                    @Override
//                    public void onSuccess(DocumentSnapshot documentSnapshot) {
//                        count = Integer.parseInt(Objects.requireNonNull(Objects.requireNonNull(documentSnapshot.getData()).toString()));
//                        Toast.makeText(getApplicationContext(),"User count: "+count,Toast.LENGTH_LONG).show();
//                    }
//                })
//                .addOnFailureListener(new OnFailureListener() {
//                    @Override
//                    public void onFailure(@NonNull Exception e) {
//                        Toast.makeText(getApplicationContext(),"Failed to get user count. Error: "+e.toString(),Toast.LENGTH_LONG).show();
//                    }
//                });

        local_signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                db.collection("user_count")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if(task.isSuccessful()){
                                    for(DocumentSnapshot documentSnapshot : Objects.requireNonNull(task.getResult())){
                                        count = Objects.requireNonNull(documentSnapshot.getLong("number")).intValue();
                                        count+=1;
//                                        Toast.makeText(getApplicationContext(),"User count: "+count,Toast.LENGTH_LONG).show();
                                    }
                                }
                                else{
                                    Toast.makeText(getApplicationContext(),"Failed to get user count. Error: ",Toast.LENGTH_LONG).show();
                                }
                            }
                        });
                username_signup = signup_username.getText().toString();
                email_signup = signup_email.getText().toString();
                password_signup = signup_password.getText().toString();

                if (TextUtils.isEmpty(username_signup)) {
                    Toast.makeText(getApplicationContext(), "Please enter username", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                if (TextUtils.isEmpty(email_signup)) {
                    Toast.makeText(getApplicationContext(), "Please enter email id", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                if(!android.util.Patterns.EMAIL_ADDRESS.matcher(email_signup).matches()){
                    Toast.makeText(getApplicationContext(), "Please enter valid email id", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                if (TextUtils.isEmpty(password_signup)) {
                    Toast.makeText(getApplicationContext(), "Enter Password", Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    return;
                }

                mAuth.createUserWithEmailAndPassword(email_signup, password_signup)
                        .addOnCompleteListener(LoginSignup.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    progressBar.setVisibility(View.GONE);
                                    Map<String, Object> user = new HashMap<>();
                                    Toast.makeText(getApplicationContext(), "Successfully created account! Please login to continue",Toast.LENGTH_LONG).show();
                                    Intent change_to_login = new Intent(LoginSignup.this, LoginSignup.class);
                                    change_to_login.putExtra("action","login");
                                    startActivity(change_to_login);
                                    FirebaseUser firebase_user = mAuth.getCurrentUser();
                                    assert firebase_user != null;
                                    user.put("uid",firebase_user.getUid());
                                    user.put("uname",username_signup);
                                    user.put("email", Objects.requireNonNull(firebase_user.getEmail()));
                                    user.put("first_time",1);
                                    db.collection("users").document("user_"+count)
                                            .set(user)
                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                @Override
                                                public void onSuccess(Void aVoid) {
//                                                    Toast.makeText(getApplicationContext(),"Data added successfully",Toast.LENGTH_LONG).show();
                                                    db.collection("user_count").document("count")
                                                            .update("number",count)
                                                            .addOnSuccessListener(new OnSuccessListener<Void>() {
                                                                @Override
                                                                public void onSuccess(Void aVoid) {
//                                                                    Toast.makeText(getApplicationContext(),"Successfully incremented the count",Toast.LENGTH_LONG).show();
                                                                }
                                                            })
                                                            .addOnFailureListener(new OnFailureListener() {
                                                                @Override
                                                                public void onFailure(@NonNull Exception e) {
                                                                    Toast.makeText(getApplicationContext(),"Failed to increment the count",Toast.LENGTH_LONG).show();
                                                                }
                                                            });
                                                }
                                            })
                                            .addOnFailureListener(new OnFailureListener() {
                                                @Override
                                                public void onFailure(@NonNull Exception e) {
                                                    Toast.makeText(getApplicationContext(),"Failed to add data. Error: "+e.toString(),Toast.LENGTH_LONG).show();
                                                }
                                            });
                                } else {
                                    // If sign in fails, display a message to the user.
                                    Toast.makeText(getApplicationContext(), "createUserWithEmail:failure "+ task.getException(),Toast.LENGTH_LONG).show();
                                    Toast.makeText(LoginSignup.this, "Authentication failed.",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

        Intent loginsignup = getIntent();
        action = loginsignup.getStringExtra("action");

        if(action.equals("signup")){
            login.setVisibility(View.GONE);
            signup.setVisibility(View.VISIBLE);

        }
        else{
            login.setVisibility(View.VISIBLE);
            signup.setVisibility(View.GONE);

        }

        logn_button_logintab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login.setVisibility(View.VISIBLE);
                signup.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Login Button Login Tab", Toast.LENGTH_LONG).show();
            }
        });

        login_button_signuptab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login.setVisibility(View.VISIBLE);
                signup.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Login Button Signup Tab", Toast.LENGTH_LONG).show();
            }
        });

        signup_button_logintab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login.setVisibility(View.GONE);
                signup.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Signup Button Login Tab", Toast.LENGTH_LONG).show();
            }
        });

        signup_button_signuptab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login.setVisibility(View.GONE);
                signup.setVisibility(View.VISIBLE);
                Toast.makeText(getApplicationContext(), "Signup Button Signup Tab", Toast.LENGTH_LONG).show();
            }
        });
    }
}
