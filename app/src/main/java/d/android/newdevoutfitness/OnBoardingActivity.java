package d.android.newdevoutfitness;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

public class OnBoardingActivity extends AppCompatActivity {

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    static int page = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.black_trans80));
        }
        setContentView(R.layout.activity_on_boarding);

        final Drawable image1 = ContextCompat.getDrawable(this, R.drawable.on_board_1);
        final Drawable image2 = ContextCompat.getDrawable(this, R.drawable.onboard_2);
        final Drawable image3 = ContextCompat.getDrawable(this, R.drawable.onboard_3);


//        final Drawable[] imageList = new Drawable[]{image1, image2, image3};

        ImageView zero = findViewById(R.id.dots_0);
        ImageView one = findViewById(R.id.dots_1);
        ImageView two = findViewById(R.id.dots_2);

        final ImageView[] dots = new ImageView[]{zero, one, two};

        SectionsPagerAdapter mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setCurrentItem(page, true);
        dots[0].setBackgroundResource(R.drawable.dot_selected);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {
            }

            @Override
            public void onPageSelected(int i) {

                page = i;
                updateIndicators(page);

                switch (i){
                    case 0:
                        mViewPager.setBackground(image1);
                        break;
                    case 1:
                        mViewPager.setBackground(image2);
                        break;
                    case 2:
                        mViewPager.setBackground(image3);
                        break;
                }
            }

            private void updateIndicators(int page) {
                for (int i = 0; i < 3; i++) {
                    dots[i].setBackgroundResource(
                            i == page ? R.drawable.dot_selected : R.drawable.dot_unselected
                    );
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_on_boarding, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        ImageView img, shade;
        String[] labels = new String[]{"Page 1", "Page 2", "Page 3"};
        String[] body = new String[]{"Some text over here", "And some over here", "And also here"};
        int[] img_bgs = new int[]{R.drawable.on_board_1, R.drawable.onboard_2, R.drawable.onboard_3};
        int background_shade = R.drawable.backgroundshade;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_on_boarding, container, false);
            TextView section_label = rootView.findViewById(R.id.section_label);
            TextView section_text = rootView.findViewById(R.id.section_text);
            ImageView get_started_button_image = rootView.findViewById(R.id.get_started_button_image);
            ImageView get_started_button_text = rootView.findViewById(R.id.get_started_button_text);
            Button get_started_button = rootView.findViewById(R.id.get_started_button);
            get_started_button_image.setVisibility(View.GONE);
            get_started_button_text.setVisibility(View.GONE);
            get_started_button.setClickable(false);
            img = rootView.findViewById(R.id.section_img);
            shade = rootView.findViewById(R.id.shade);
            assert getArguments() != null;
            if(getArguments().getInt(ARG_SECTION_NUMBER) - 1 == 2){
                get_started_button.setClickable(true);
                get_started_button_image.setVisibility(View.VISIBLE);
                get_started_button_text.setVisibility(View.VISIBLE);
                section_text.setVisibility(View.INVISIBLE);
                get_started_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Utils.saveSharedSetting(Objects.requireNonNull(PlaceholderFragment.this.getActivity()), MainActivity.PREF_USER_FIRST_TIME, "false");
                        Intent login_activity = new Intent(v.getContext(), MainActivity.class);
                        startActivity(login_activity);
                    }
                });
            }
            section_label.setText(labels[getArguments().getInt(ARG_SECTION_NUMBER) - 1]);
            if(getArguments().getInt(ARG_SECTION_NUMBER) - 1 != 2)
                section_text.setText(body[getArguments().getInt(ARG_SECTION_NUMBER) - 1]);
            img.setBackgroundResource(img_bgs[getArguments().getInt(ARG_SECTION_NUMBER) - 1]);
            shade.setBackgroundResource(background_shade);
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
}
